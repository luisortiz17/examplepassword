package com.cognitus.examplepassword

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.cognitus.examplepassword.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val sharedPreference = SharedPreference(this)
        //setContentView(R.layout.activity_main)

        binding.setClickListener {
            when (it!!.id) {
                binding.btnLogin.id -> {
                    intent = Intent(applicationContext, FirmaActivity::class.java)
                    startActivity(intent)
                }

                binding.btnCamera.id -> {
                    intent = Intent(applicationContext, CameraActivity::class.java)
                    startActivity(intent)
                }

                binding.btnService.id -> {
                    intent = Intent(applicationContext, ListadoActivity::class.java)
                    startActivity(intent)
                }

                binding.btnSave.id -> {
                    sharedPreference.save("name", "prueba de nombre")
                    Toast.makeText(this, "Datos guardados", Toast.LENGTH_SHORT).show()
                }
                binding.btnVer.id -> {
                    if (sharedPreference.getValueString("name") != null) {
                        Toast.makeText(
                                this,
                                "Datos guardados->" + sharedPreference.getValueString("name"),
                                Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        Toast.makeText(this, "No hay datos", Toast.LENGTH_SHORT).show()
                    }

                }

                binding.btnDialog.id -> {
                    AlertDialogCustom.createAlert(this,"Titulo","Hola es un mensaje")
                }
            }
        }
    }
}